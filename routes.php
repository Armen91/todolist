<?php
	$route->addRoute('/', 'HomeController@index');
	$route->addRoute('/login', 'LoginController@index');
	$route->addRoute('/home', 'HomeController@index');
	$route->addRoute('/admin', 'AdminController@index');
	$route->addRoute('/admin/*', 'AdminController@index');
	$route->addRoute('/error404', 'ErrorController@Error404');
	$route->addRoute('/logout', 'LogoutController@index');
	$route->addRoute('/addproject', 'AddProjectController@index');
	$route->addRoute('/printtask', 'AddTaskController@print');
	$route->addRoute('/addtask', 'AddTaskController@add');
	$route->addRoute('/days', 'DaysController@index');
	$route->addRoute('/search', 'SearchController@index');
?>