-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 04, 2019 at 04:34 PM
-- Server version: 5.7.25-0ubuntu0.18.04.2
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `todo_bs`
--

-- --------------------------------------------------------

--
-- Table structure for table `administartors`
--

CREATE TABLE `administartors` (
  `admin_id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

CREATE TABLE `categorys` (
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categorys`
--

INSERT INTO `categorys` (`category_id`, `user_id`, `content`) VALUES
(1, 11, 'Work'),
(2, 11, 'Personal'),
(3, 11, 'Hobby'),
(4, 11, 'Studying'),
(5, 11, 'Mstudying'),
(6, 11, 'Ashot'),
(7, 11, 'Aram'),
(8, 11, 'test1');

-- --------------------------------------------------------

--
-- Table structure for table `cookies`
--

CREATE TABLE `cookies` (
  `cookie_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `cookie_key` varchar(255) NOT NULL,
  `cookie_name` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cookies`
--

INSERT INTO `cookies` (`cookie_id`, `user_id`, `cookie_key`, `cookie_name`, `time`) VALUES
(1, 11, 'gajYeMDogIqejyHnuKoC0AWb24QyY3bCYlMRy1Vw6yQLF7QQMN', 'login', 'false'),
(6, 11, 'GbSzTva9PD9Yy1BaitVi6BfTyxnMa4Hp7DhMYkJa3z6zPk33jw', 'login', 'false'),
(9, 11, 'HqPwhKExD93X3amQw318Wt7kZQbgaNqjXfjTIR1OPMgB1Viu9C', 'login', 'false'),
(10, 11, 'zyi46axgvgmKNGGCIkciwpcoKuObGCYaoCC6RfKWdgIkUYrcA3', 'login', 'false'),
(15, 11, 'noaiDXeZkl3Axwx6GboNHGL0G1f5481iiBgz6uxXIRm97oKoBp', 'login', 'false');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `notfication_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `notification_text` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL,
  `user_id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `user_id`, `title`, `category_id`, `created_at`) VALUES
(1, 11, 'Create Table', 1, '2019-02-01 13:51:03'),
(2, 11, 'Hashtkni', 1, '2019-02-01 13:53:48'),
(3, 11, 'Craetiv', 1, '2019-02-01 13:55:59'),
(4, 11, 'Beautiful', 1, '2019-02-01 13:58:02'),
(5, 11, 'Soon', 1, '2019-02-01 13:59:12'),
(6, 11, 'Personal', 2, '2019-02-01 17:39:40'),
(7, 11, 'Hobby', 3, '2019-02-01 17:42:05'),
(8, 11, 'Studying', 4, '2019-02-01 17:43:13'),
(9, 11, 'Study mstudy', 4, '2019-02-02 18:55:43'),
(10, 11, 'Hobby mobby ', 3, '2019-02-02 18:57:28'),
(11, 11, 'Personal mersonal', 2, '2019-02-02 18:58:58'),
(12, 11, 'hhhhh', 2, '2019-02-02 18:59:20'),
(13, 11, 'Moon', 1, '2019-02-02 18:59:35'),
(14, 11, 'Ashot', 1, '2019-02-04 07:36:12'),
(15, 11, 'fdsfsdfdsf', 1, '2019-02-04 08:35:19'),
(16, 11, 'vvvvvvv', 1, '2019-02-04 09:15:33'),
(17, 11, 'test1', 1, '2019-02-04 10:40:31'),
(18, 11, 'test2', 1, '2019-02-04 10:42:45');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `task_id` int(11) NOT NULL,
  `user_id` int(10) NOT NULL,
  `project_id` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `deadline` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '3',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`task_id`, `user_id`, `project_id`, `title`, `description`, `deadline`, `priority`, `created_at`) VALUES
(1, 11, 1, 'Create table fild->\'users\'', '', '', 0, '2019-02-02 16:03:06'),
(2, 11, 1, 'Creat esim inch', '', '', 0, '2019-02-02 18:52:49'),
(4, 11, 4, 'Beautiful task', '', '', 0, '2019-02-02 18:54:08'),
(5, 11, 1, 'Ashot`s task', '', '', 0, '2019-02-04 07:36:55'),
(6, 11, 1, 'sadasdasd', '', '', 0, '2019-02-04 08:35:43'),
(7, 11, 2, 'sdasdasd', '', '', 0, '2019-02-04 08:50:33'),
(8, 11, 4, 'SADaDAdad', '', '', 0, '2019-02-04 08:59:57'),
(9, 11, 1, 'wwarwqrwr', '', '', 0, '2019-02-04 09:14:12'),
(10, 11, 16, 'dddddddd', '', '', 0, '2019-02-04 09:15:43'),
(11, 11, 6, 'sdsdsdsdsd person', '', '', 0, '2019-02-04 09:55:48'),
(12, 11, 18, 'test2       1', '', '', 0, '2019-02-04 10:46:02'),
(13, 11, 1, 'sasdasdsad', '', '', 0, '2019-02-04 11:49:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `phone` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `surname`, `phone`, `email`, `password`, `created_at`) VALUES
(8, 'vagharshak', '', 0, 'user@user.user', 'password', '2019-01-29 12:44:47'),
(11, 'armen', '', 0, 'user@user.user', 'password', '2019-01-29 14:42:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administartors`
--
ALTER TABLE `administartors`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `categorys`
--
ALTER TABLE `categorys`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `cookies`
--
ALTER TABLE `cookies`
  ADD PRIMARY KEY (`cookie_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notfication_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administartors`
--
ALTER TABLE `administartors`
  MODIFY `admin_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categorys`
--
ALTER TABLE `categorys`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `cookies`
--
ALTER TABLE `cookies`
  MODIFY `cookie_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `notfication_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
