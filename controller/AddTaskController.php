<?php
	class AddTaskController extends Controller{
		
		public function print(){
			$cookies = $this->model('Cookies');
			$user_id = $cookies->select('user_id', ['cookie_key'=>$_COOKIE['login']]);
			$tasks = $this->model('Tasks');
			if(isset($this->request->post['project_id'])){

			
			$result = $tasks->select('task_id, title', ['project_id'=>$this->request->post['project_id']]);
			if($result['num_rows'] == 0){
				echo 0; die;
			}else{
				echo json_encode($result['rows']); die;
			}
		}
			if(isset($this->request->post['priority'])){
				$priority = $this->request->post['priority'];
				$cookies = $this->model('Cookies');
				$user_id = $cookies->select('user_id', ['cookie_key'=>$_COOKIE['login']]);
				if($priority=='all'){
				$task = $tasks->select('task_id, title', ['user_id' => $user_id]);
				if($task){
					echo json_encode($task['rows']); die;
				}
				}else{
					if($priority=='no_dead'){
					$task = $tasks->select('task_id, title', ['user_id' => $user_id, 'deadline'=>'']);
				if($task){
					echo json_encode($task['rows']); die;
				}	
					}else{
				$task = $tasks->select('task_id, title', ['user_id' => $user_id, 'priority'=> $priority]);
				if($task){
					echo json_encode($task['rows']); die;
				}}
			}}
			

			if(isset($this->request->post['current_time'])){
				
				$current_time = $this->request->post['current_time'];
				$res = $tasks->select('task_id, title, deadline', ['user_id' => $user_id]);
				if($res){
					echo json_encode($res['rows']);die;
				}
			}
			
		}
		

		public function add(){
			$cookies = $this->model('Cookies');
			$tasks = $this->model('Tasks');
			$data['user_id'] = $cookies->select('user_id', ['cookie_key'=>$_COOKIE['login']]);
			$data['project_id'] = $this->request->post['project_id'];
			$data['title'] = $this->request->post['title'];
			$data['only_date'] = $this->request->post['only_date'];
			$data['deadline'] = $this->request->post['deadline'];
			$data['priority'] = $this->request->post['prio'];
			$result = $tasks->insert($data);
			if($result){
				echo 1;
			}else{
				echo 0;
			}
		}


		
	}