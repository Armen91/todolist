<?php
	class AddProjectController extends Controller{
		public function index(){
			$projects = $this->model('Projects');
			$cookies = $this->model('Cookies');
			if($this->request->method == 'POST' && isset($this->request->post['save'])){
				$array['user_id'] = $cookies->select('user_id', ['cookie_key'=>$_COOKIE['login']]);
				$array['title'] = $this->request->post['title'];
				$array['category_id'] = $this->request->post['category_id'];
				$result = $projects->insert($array);
				$res = $projects->select('project_id,title', ['user_id' => $array['user_id']]);
				if($res){
					 print_r(json_encode($res['rows']));
				}else{
					echo 0;
				}
			}

				if($this->request->method == 'POST' && isset($this->request->post['delete_id'])){
					$del_id = $this->request->post['delete_id'];
					$tasks = $this->model('Tasks');
					$del_tasks = $tasks->delete(['project_id'=>$del_id]);
					$del = $projects->delete(['project_id'=>$del_id]);
					if($del){
						echo 1;die;
					}else{
						echo 0;die;
					}
				}
			
		}
	}