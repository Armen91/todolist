<?php
	class HomeController extends Controller{
		public function index(){
			$cookies = $this->model('Cookies');
			$categorys = $this->model('Categorys');
			$projects = $this->model('Projects');
			$tasks = $this->model('Tasks');
			if(!isset($_COOKIE['login'])){
				redirect('/login');
			}else if(!$user_id = $cookies->select('user_id', ['cookie_key'=>$_COOKIE['login']])){
					redirect ('/login');
				}else{
					$user_id = $cookies->select('user_id', ['cookie_key'=>$_COOKIE['login']]);
				}

				$sel = $projects-> select('title, project_id', ['user_id' => $user_id]);
				$count = $sel['num_rows'];
				$ar = [];
				for($i = 0; $i < $count; $i++){
					
				 	 $ar[] = $sel['rows'][$i];
				 }
				 $curent_date = date('d-m-Y');
				 $select_task = $tasks->select('task_id, title', ['user_id'=> $user_id,'only_date' => $curent_date]);
				 $tasks_count = count($select_task['rows']);
				 if(isset($select_task['rows']) && !empty($select_task['rows'])){
				 for($j = 0; $j<$tasks_count; $j++){
					 $today_tasks[] = $select_task['rows'][$j];
				 }

					$data['to_day'] = $today_tasks;
				}
			$data['arr'] = $ar;
			$header = $this->view('header');
			$footer = $this->view('footer');
			$data['header'] = $header;
			$data['footer'] = $footer;
			$this->setOutput('home', $data);
		}
	}
?>