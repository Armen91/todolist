$(function () {



    $('.animateButton').on('click', function () {

        $('.animateSpan').toggleClass('open');
      });


    $("#myBtn").click(function(){
        $("#myModal").modal();
    });

    $("#myBtnRegistr").click(function(){
        $("#myModalRegistr").modal();
    });

    $("#addPlus").click(function(){
        $("#myAddPlus").modal();
    });

    $(document).on('click', '#addTask',function() {
        $('.addTaskCenter').css('display', 'block');
        $('.taskCenter').css('display', 'none');
        var pr_id = $(this).data('project-id');
        console.log(pr_id);
        $('#saveTask').attr('data-project-id',pr_id);
    })

    $('#saveTask').click(function(){
        $('.addTaskCenter').css('display', 'none');
        $('.taskCenter').css('display', 'block');
    })

    $('#cencelTask').click(function(){
        $('.addTaskCenter').css('display', 'none');
        $('.taskCenter').css('display', 'block');
    })

    
    
Data = new Date();
Week = Data.getDay();
Month = Data.getMonth();
Day = Data.getDate();

switch (Week)
{
  case 0: fWeek="Sun"; break;
  case 1: fWeek="Mon"; break;
  case 2: fWeek="Tue"; break;
  case 3: fWeek="Wed"; break;
  case 4: fWeek="Thu"; break;
  case 5: fWeek="Fri"; break;
  case 6: fWeek="Sat"; break;
}


switch (Month)
{
  case 0: fMonth="Jan"; break;
  case 1: fMonth="Feb"; break;
  case 2: fMonth="Mar"; break;
  case 3: fMonth="Apr"; break;
  case 4: fMonth="May"; break;
  case 5: fMonth="Jun"; break;
  case 6: fMonth="Jul"; break;
  case 7: fMonth="Aug"; break;
  case 8: fMonth="Sep"; break;
  case 9: fMonth="Oct"; break;
  case 10: fMonth="Nov"; break;
  case 11: fMonth="Dec"; break;
}
 
// Вывод
$('#date').append(" "+Day+" "+fMonth+" "+fWeek+" ");
$('#dateAdd').append(" "+Day+" "+fMonth+" ");
$('#dateAddTask').append(" "+Day+" "+fMonth+" ");




    $('.panel-heading').click(function () {
        $(this).toggleClass('in').next().slideToggle();
        // $('.panel-heading').not(this).removeClass('in').next().slideUp();
    });

    $('.button').click(function(){
       category_id = $(this).attr('data-category-id');
        $('.divProject').remove();
        $(this).after("<div class='divProject'><input class='inputProject' type='text' placeholder='Add Project'><div><button type='button' class='btn btn-success saveProject' data-category-id="+category_id+">Save</button><button type='button' class='btn btn-danger cancelProject'>Cancel</button></div></div>");
        $(this).css('display', 'none');
    });



    $('.panelSetings').click(function () {
        $('.tableSettings').toggle(200);
        $('.spanSettings').toggle(100);
    });

    $('.panelNotifi').click(function () {
        $('.tableNotifi').toggle(200);
        $('.spanSettings1').toggle(100);
    });

    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".tableSettings, .spanSettings, .tableNotifi, .panelSetings"); // тут указываем ID элемента
        var div1 = $(".tableSettings, .spanSettings");
        if (!div.is(e.target) // е сли клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            div1.hide(); 
        }
        var div2 = $(".tableSettings, .spanSettings1, .tableNotifi, .panelNotifi"); // тут указываем ID элемента
        var div3 = $(".tableNotifi, .spanSettings1");
        if (!div2.is(e.target) // е сли клик был не по нашему блоку
            && div2.has(e.target).length === 0) { // и не по его дочерним элементам
            div3.hide(); 
        }
    });

    $('.menuBut').on('click', function () {

    $('.menuSpan').toggleClass('open');
    });

    var h = $(window).height();
    $(".akkordionOne").css('height', h - 50 + 'px');


 
});



    $( window ).resize(function() {
    var h = $(window).height();
    $(".akkordionOne").css('height', h - 50 + 'px');
});
   



$(document).on('click', '.cancelProject', function(){
   $('.divProject').remove();
   $('.button').css('display', 'block');
})

$(document).on('click', '.saveProject', function(){
    var this_tag = $(this);
    var category_id = $(this).attr('data-category-id');
    var title = $('.inputProject').val();
    var save = '';
    $.ajax({
        url:'/addproject',
        method: 'post', 
        data:{title:title, save:save, category_id:category_id},
        dataType: 'json',
        success: function(r){
            if(r){
                this_tag.parent().parent().parent().before('<li><a class="project" data-project-id="'+ r[r.length-1].project_id +'">'+title+'</a><button type="button" class="close projectCloce" data-dismiss="modal" data-prId="'+ r[r.length-1].project_id +'">×</button></li>');
                $('.divProject').remove();
                $('.button').css('display', 'block');
            }
            if(r == 0){
                $('.divProject input').after('<p class="error">There is a problem with server</p>');
            }
        }
    })

})
$(document).on('click', '.project', function(){
    var h1 = $(this).text();
    var project_id = $(this).attr('data-project-id');
    $('.ulCenter').attr('id', project_id);
    $.post({
        url:'/printtask',
        data:{project_id:project_id},
        success:function(res){
            if(res == 0){
                $('.ulCenter').empty();
                $('.ulCenter').html('<li><h1>'+h1+'</h1></li><li class="taskCenter"><a href="#" id="addTask" data-project-id="'+ project_id +'">Add Task</a></li>')
            }else{
                console.log('yes');
                var data = JSON.parse(res);
                html = '';
                $(data).each(function(){
                    html += '<li><a data-task-id="'+$(this)[0].task_id+'">'+$(this)[0].title+'</a></li>';
                })
                html += '<li class="taskCenter"><a href="#" id="addTask" data-project-id="'+ project_id +'">Add Task</a></li>';
                $('.ulCenter').html('<li><h1>'+h1+'</h1></li>' + html);
                
            }
        }
    })
})
$(document).on('click', '.button_task', function(){
        var project_id = $(this).attr('data-project-id');
        $('.divProject').remove();
        $(this).after("<div class='divProject'><input class='addTaskInput' type='text' placeholder='Add Task'><div><button type='button' class='btn btn-success saveTask' data-project-id="+project_id+">Save</button><button type='button' class='btn btn-danger cancelProject'>Cancel</button></div></div>");
        $(this).css('display', 'none');
    });


    $(document).on('click','.dropdown-item', function(){
        var prio = $(this).data('priority');
        $('.saveTask').attr("data-priority", prio);
    })


$(document).on('click', '.saveTask', function(){
    var this_tag = $(this);
    var project_id = $(this).attr('data-project-id');
    var title = $('.addTaskInput').val();
     var  pr = $(this).data("priority");
     var deadline = $('#deadline_date').val();
     var only_date = deadline.slice(0,10);
    $('.taskCenter').before('<li><a>'+title+'</a></li>');
    $.post({
        url:'/addtask',
        data:{project_id:project_id, title:title, prio:pr, deadline: deadline, only_date: only_date},
        success:function(res){
            if(res == 1){
                $('#content ul').prepend('<li><a>'+title+'</a></li>');
                $('.divProject').remove();
                $('.button_task').css('display', 'block');
            }
            if(res == 0){
                $('.divProject input').after('<p class="error">There is a problem with server</p>');
            }
        }
    })
})

$('.addGroup').click(function(){
    $(this).after('<div class="add_input"><input type="text" placeholder="Add Category" class="inp_add"><div class="add_inp_buttons"><button class="save">Save</button><button class="cancel">Cancel</button></div></div>')
    $(this).css('display', 'none');
})

$(document).on('click','.cancel', function(){
    $('.addGroup').css('display', 'block');
    $('.add_input').remove();
})

$(document).on('click', '.save',function (event) {
    event.preventDefault();
    var data = $('.inp_add').val()
    $('.addGroup').css('display', 'block');
    $('.add_input').remove();
    //console.log(data);
  $.ajax({
       url: "/home",
       method: 'post',
       data: {'title': data},
       success : function(da){
           da = JSON.parse(da);
           var el = da.rows;
           $('.panel:last').after('<div class="panel"><div class="panel-heading"><a><i class="fa fa-shopping-cart"></i>'+el[el.length-1]["content"] +'</a></div><div class="panel-collapse"><div class="panel-body"><div class="wrapper-ul"><ul><li><div class="button"><a>Add Work page</a></div></li></ul></div></div></div></div>')
           console.log(el[el.length-1]["content"]);
       }
   })

   

});

$(document).on('click', '.projectCloce', function(){
    var delete_id = $(this).data('prid');
    $(this).parent().remove();
    
        $.post({
        url: "/addproject",
        data: {delete_id: delete_id },
        success : function(e){
            console.log($('.ulCenter').attr('id'));
             if($('.ulCenter').attr('id') == delete_id){
                 $('.ulCenter').empty();
            }
        }
})
})

$('.priority').on('click', function(){
    var filtr = $(this).data('set');
    $.post({
        url: "/printtask",
        data: {priority: filtr },
        success : function(e){
            if(e){
            e = JSON.parse(e);
            var tasks = '';
            console.log(e);
            for($i = 0; $i < e.length; $i++){
                tasks += '<li><a data-task-id="'+$(e)[$i].task_id+'">'+$(e)[$i].title+'</a></li>'
            }
              $('.ulCenter').empty();
              $('.ulCenter').append(tasks)  
            }
        }
})
})

$('#dtBox').DateTimePicker();

$('#to_day').click(function(){
    var current_time = $(this).data('current');
    $('.ulCenter').empty();
    $.post({
        url: '/printtask',
        data: {current_time: current_time},
        dataType: 'json',
        success: function(result){
            var res_count = result.length
            console.log(res_count);
            for($i = 0; $i < res_count; $i++){
                 var date = result[$i]['deadline']
                 date = date.slice(0,10);
                 if(date == current_time){
                    $('.ulCenter').prepend('<li><a data-task-id="'+ result[$i]['task_id'] +'">'+ result[$i]['title'] +'</a></li>')
                 }
            }
        }
    })
})

$('#next_7').click(function(){
    var save = '';
    $.post({
        url: '/days',
        data: {save: save},
        dataType: 'html',
        success: function(h){
            $('.ulCenter').html(h);
        }
    })
})



function fill(value){
    $('#search').val(value);
    $('#searchdisplay').hide();
}

$(document).on('click', ".searchli", function() {
    var clickli=$(this).text();
    $('#searchdisplay').hide();
    $('#search').val(clickli);
     var search=$("#search").val();
        var html = '';
        if(search==""){
            $("#searchdisplay").html("");
        }else{
            $.ajax({
                type: "POST",
                url: "/search",
                data: {
                search: search
                },
                dataType: 'json',
                success: function(result){
                    console.log(result);

                for(i=0; i<result.length; i++){ //'title', 'task_id', 'created_at', 'priority', 'deadline'
                   html += "<li class='searchresalt'"+ result[i]['task_id'] +"'><h3>"
                    +result[i]['title']+'<br>Priority --> '+result[i]['priority']+'<br>Created At --> '+result[i]['created_at']+'<br>Deadline --> '+result[i]['deadline']+
                    "</h3></li>";
                   
                }
                $(".ulCenter").html('<ul>' + html + '</ul>').show();  
                $("#search").val(""); 
                }
            })
        }
});

$(document).on('mouseover', ".searchli", function() {
    var hoverli=$(this).text();
    $('#search').val(hoverli);
});

$(document).ready(function() {
    $("#search").keyup(function(){
        var search=$("#search").val();
        var html = '';
        if(search==""){
            $("#searchdisplay").html("");
        }else{
            $.ajax({
                type: "POST",
                url: "/search",
                data: {
                search: search
                },
                dataType: 'json',
                success: function(result){
                    console.log(result);
                
                   

                for(i=0; i<result.length; i++){
                   html += "<li class='searchli' data-task-id='"+ result[i]['task_id'] +"'>"+result[i]['title']+"</li>";
                }
                $("#searchdisplay").html('<ul>' + html + '</ul>').show();
                }
            })
        }
    })
})